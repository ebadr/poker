This application takes a file as input and prints the result on console.
The input is a file where each line contain a poker hand.
cards on each line are separated with space.
each line contain only 5 cards.
The application is sensitive for extra characters or white spaces
The output is the input hands ordered in descending order based on poker hand power rules

How to run:
use the jar poker.jar
start the app using the command java -jar poker.jar input
where input is the input file name

There is a sample input file (input.txt) included

Improvement areas:
- validations
- improving parser error handling
- more detailed unit tests
- Separation for the view layer

