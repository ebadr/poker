package poker.types;

import java.util.Collections;
import java.util.List;

import poker.util.HandScoreCalculator;

public class Hand implements Comparable<Hand> {
    public static final int HAND_MAX_SIZE = 5;
    private List<Card> cards = null;

    private HandScore score = null;

    public Hand(List<Card> cards) { // input validation could be added here but
				    // for now will depend on the upper layer
	this.cards = cards; // keeping the original order for printing purpose
	this.score = HandScoreCalculator.calculateHandScore(cards);
    }

    @Override
    public int compareTo(Hand h) { // compare score to be used in collections
				   // sorting
	int result = -1;
	if (h != null) {
	    result = this.score.compareTo(h.score);
	}
	return result;
    }

    public List<Card> getCards() {
	return Collections.unmodifiableList(cards);
    }

    public HandScore getScore() {
	return score;
    }

    @Override
    public String toString() {
	StringBuilder result = new StringBuilder();
	for (Card card : cards) {
	    result.append(card.toString() + " ");
	}
	result.append(this.score.getHandPower().toString());
	return result.toString();
    }
}
