package poker.types;

public enum Suit {
    HEARTS("H"), DIAMONDS("D"), CLUBS("C"), SPADES("S");

    private final String symbol;

    private Suit(String symbol) {
	this.symbol = symbol;
    }

    @Override
    public String toString() {
	return symbol;
    }

}
