package poker.types;

import java.util.Collections;
import java.util.List;

public class HandScore implements Comparable<HandScore> {
    private HandPower handPower = null;
    private List<Rank> cardsPower = null;

    public HandScore(HandPower handPower, List<Rank> cardsPower) {
	this.handPower = handPower;
	this.cardsPower = cardsPower;
    }

    public List<Rank> getCardsPower() {
	return Collections.unmodifiableList(cardsPower);
    }

    public HandPower getHandPower() {
	return handPower;
    }

    @Override
    public int compareTo(HandScore o) {
	int handPowerDiff = -1;
	if (o != null) {
	    handPowerDiff = this.handPower.compareTo(o.handPower);
	    if (handPowerDiff == 0) {
		int cardsPowerDiff = 0;
		for (int i = 0; i < this.cardsPower.size() && i < o.getCardsPower().size(); i++) {
		    cardsPowerDiff = this.cardsPower.get(i).compareTo(o.getCardsPower().get(i));
		    if (cardsPowerDiff != 0) {
			return cardsPowerDiff;
		    }
		}
	    }
	}
	return handPowerDiff;
    }
}
