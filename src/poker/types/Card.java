package poker.types;

public class Card implements Comparable<Card> {
    private Rank rank;
    private Suit suit;

    public Rank getRank() {
	return rank;
    }

    public Suit getSuit() {
	return suit;
    }

    public Card(Rank rank, Suit suit) {
	this.rank = rank;
	this.suit = suit;

    }

    @Override
    public boolean equals(Object card) {
	if (card instanceof Card) {
	    return this.rank.equals(((Card) card).getRank()) && this.suit.equals(((Card) card).getRank());
	}
	return false;
    }

    @Override
    public String toString() {

	return this.rank.toString() + this.suit.toString();
    }

    @Override
    public int compareTo(Card o) {
	return this.rank.compareTo(o.rank);
    }

}
