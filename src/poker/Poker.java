package poker;

import java.util.Collections;
import java.util.List;

import poker.parser.FileParser;
import poker.types.Hand;

public class Poker {

    public static void main(String[] args) {
	if (args.length > 0) {
	    FileParser parser = new FileParser();
	    try {
		List<Hand> handsList = parser.parse(args[0]);
		Collections.sort(handsList);
		for (Hand hand : handsList) {
		    System.out.println(hand);
		}
	    } catch (Exception e) {
		System.err.println("Erorr Parsing Input");
		e.printStackTrace();
	    }
	} else {
	    System.err.println("No input file name");
	}

    }

}
