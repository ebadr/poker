package poker.parser;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import poker.types.Card;
import poker.types.Hand;
import poker.types.Rank;
import poker.types.Suit;

public class FileParser {

    public List<Hand> parse(String file) throws Exception {
	String content = new String(Files.readAllBytes(Paths.get(file)));
	String[] lines = content.split("\n");
	List<Hand> handsList = new ArrayList<Hand>();
	for (int i = 0; i < lines.length; i++) {
	    lines[i] = lines[i].replaceAll("\r", "");
	    handsList.add(parseHandLine(lines[i]));
	}
	return handsList;
    }

    private Hand parseHandLine(String line) throws Exception {
	String[] cards = line.split(" ");
	if (cards.length != 5) {
	    throw new Exception();
	}
	List<Card> cardsList = new ArrayList<Card>();
	for (int i = 0; i < 5; i++) {
	    cardsList.add(parsCardString(cards[i]));
	}
	Hand hand = new Hand(cardsList);
	return hand;

    }

    private Card parsCardString(String input) throws Exception {
	String[] cardStrings = input.split("");
	if (cardStrings.length != 2) {
	    throw new Exception("Erorr parsing Card: " + input);
	}
	Card card = new Card(parseRank(cardStrings[0]), parseSuit(cardStrings[1]));
	return card;
    }

    private Rank parseRank(String input) throws Exception {
	Rank rank = null;
	switch (input) {
	case "A":
	    rank = Rank.ACE;
	    break;
	case "K":
	    rank = Rank.KING;
	    break;
	case "Q":
	    rank = Rank.QUEEN;
	    break;
	case "J":
	    rank = Rank.JACK;
	    break;
	case "T":
	    rank = Rank.TEN;
	    break;
	case "9":
	    rank = Rank.NINE;
	    break;
	case "8":
	    rank = Rank.EIGHT;
	    break;
	case "7":
	    rank = Rank.SEVEN;
	    break;
	case "6":
	    rank = Rank.SIX;
	    break;
	case "5":
	    rank = Rank.FIVE;
	    break;
	case "4":
	    rank = Rank.FOUR;
	    break;
	case "3":
	    rank = Rank.THREE;
	    break;
	case "2":
	    rank = Rank.TWO;
	    break;
	default:
	    throw new Exception("Error parsing rank: " + input);
	}
	return rank;
    }

    private Suit parseSuit(String input) throws Exception {
	Suit suit = null;
	switch (input) {
	case "C":
	    suit = Suit.CLUBS;
	    break;
	case "D":
	    suit = Suit.DIAMONDS;
	    break;
	case "H":
	    suit = Suit.HEARTS;
	    break;
	case "S":
	    suit = Suit.SPADES;
	    break;
	default:
	    throw new Exception("Error parsing suit: " + input);
	}
	return suit;
    }
}
