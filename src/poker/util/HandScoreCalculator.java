package poker.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import poker.types.Card;
import poker.types.Hand;
import poker.types.HandPower;
import poker.types.HandScore;
import poker.types.Rank;

public class HandScoreCalculator {
    public static HandScore calculateHandScore(List<Card> cards) {
	List<Card> orderedCards = new ArrayList<Card>(cards);
	Collections.sort(orderedCards);
	Collections.reverse(orderedCards);
	boolean isFlush = isFlush(cards);
	boolean isStraight = isStraight(orderedCards); // both checks can be
						       // done a single step but
						       // separated in 2 methods
						       // for simplicity
	HandScore score = null;
	if (isStraight && isFlush) {
	    score = getStraightFlushScore(orderedCards);

	} else if (isStraight) {
	    score = getStraightScore(orderedCards);
	} else if (isFlush) {
	    score = getFlushScore(orderedCards);
	} else {
	    score = getRepeatedRanksScore(orderedCards);
	}
	return score;
    }

    private static HandScore getRepeatedRanksScore(List<Card> orderedCards) {
	Map<Integer, List<Rank>> ranksRepetition = getRanksRepetition(orderedCards);
	List<Rank> cardsPower = new ArrayList<Rank>();
	HandPower handPower = HandPower.HIGH_CARD;
	if (ranksRepetition.containsKey(4)) {
	    handPower = HandPower.FOUR_OF_A_KIND;
	    cardsPower.addAll(ranksRepetition.get(4));
	    cardsPower.addAll(ranksRepetition.get(1));
	} else if (ranksRepetition.containsKey(3)) {
	    cardsPower.addAll(ranksRepetition.get(3));
	    if (ranksRepetition.containsKey(2)) {
		handPower = HandPower.FULL_HOUSE;
		cardsPower.addAll(ranksRepetition.get(2));
	    } else {
		handPower = HandPower.THREE_OF_A_KIND;
		cardsPower.addAll(ranksRepetition.get(1));
	    }

	} else if (ranksRepetition.containsKey(2)) {
	    if (ranksRepetition.get(2).size() == 2) {
		handPower = HandPower.TWO_PAIRS;
	    } else {
		handPower = HandPower.PAIR;
	    }
	    cardsPower.addAll(ranksRepetition.get(2));
	    cardsPower.addAll(ranksRepetition.get(1));
	} else {
	    cardsPower.addAll(ranksRepetition.get(1));
	}
	return new HandScore(handPower, cardsPower);

    }

    private static HandScore getFlushScore(List<Card> orderedCards) {
	return new HandScore(HandPower.FLUSH, mapCardsToRanks(orderedCards));

    }

    private static HandScore getStraightScore(List<Card> orderedCards) {
	return new HandScore(HandPower.STRAIGHT, mapCardsToRanks(orderedCards));

    }

    private static HandScore getStraightFlushScore(List<Card> orderedCards) {
	return new HandScore(HandPower.STRAIGHT_FLUSH, mapCardsToRanks(orderedCards));
    }

    private static boolean isFlush(List<Card> cards) {
	for (int i = 0; i < Hand.HAND_MAX_SIZE - 1; i++) {
	    if (cards.get(i).getSuit() != cards.get(i + 1).getSuit()) {
		return false;
	    }
	}
	return true;
    }

    private static boolean isStraight(List<Card> orderedCards) {
	for (int i = 0; i < Hand.HAND_MAX_SIZE - 1; i++) {
	    if (orderedCards.get(i).getRank().ordinal() - orderedCards.get(i + 1).getRank().ordinal() != 1) {
		return false;
	    }
	}
	return true;
    }

    private static List<Rank> mapCardsToRanks(List<Card> cards) {
	if (cards != null) {
	    return cards.stream().map(c -> c.getRank()).collect(Collectors.toList());
	}
	return null;
    }

    private static Map<Integer, List<Rank>> getRanksRepetition(List<Card> cards) {
	List<Rank> ranks = mapCardsToRanks(cards);
	Map<Integer, List<Rank>> result = ranks.stream()
		.collect(Collectors.groupingBy(r -> Collections.frequency(ranks, r), Collectors.toList()));
	result.forEach((k, v) -> {
	    v = v.stream().distinct().collect(Collectors.toList());
	    result.put(k, v);
	});
	return result;
    }

}
