package poker.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import poker.types.Card;
import poker.types.HandPower;
import poker.types.HandScore;
import poker.types.Rank;
import poker.types.Suit;

public class HandScoreCalculatorTest {

    @Test
    public void testStraightScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.ACE, Suit.CLUBS));
	cards.add(new Card(Rank.KING, Suit.DIAMONDS));
	cards.add(new Card(Rank.JACK, Suit.CLUBS));
	cards.add(new Card(Rank.QUEEN, Suit.CLUBS));
	cards.add(new Card(Rank.TEN, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.STRAIGHT, score.getHandPower());
	assertEquals(Rank.ACE, score.getCardsPower().get(0));
	assertEquals(Rank.KING, score.getCardsPower().get(1));
	assertEquals(Rank.QUEEN, score.getCardsPower().get(2));
	assertEquals(Rank.JACK, score.getCardsPower().get(3));
	assertEquals(Rank.TEN, score.getCardsPower().get(4));
    }

    @Test
    public void testStraightFlushScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.ACE, Suit.CLUBS));
	cards.add(new Card(Rank.KING, Suit.CLUBS));
	cards.add(new Card(Rank.JACK, Suit.CLUBS));
	cards.add(new Card(Rank.QUEEN, Suit.CLUBS));
	cards.add(new Card(Rank.TEN, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.STRAIGHT_FLUSH, score.getHandPower());
	assertEquals(Rank.ACE, score.getCardsPower().get(0));
	assertEquals(Rank.KING, score.getCardsPower().get(1));
	assertEquals(Rank.QUEEN, score.getCardsPower().get(2));
	assertEquals(Rank.JACK, score.getCardsPower().get(3));
	assertEquals(Rank.TEN, score.getCardsPower().get(4));
    }

    @Test
    public void testFlushScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.ACE, Suit.CLUBS));
	cards.add(new Card(Rank.KING, Suit.CLUBS));
	cards.add(new Card(Rank.TWO, Suit.CLUBS));
	cards.add(new Card(Rank.QUEEN, Suit.CLUBS));
	cards.add(new Card(Rank.TEN, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.FLUSH, score.getHandPower());
	assertEquals(Rank.ACE, score.getCardsPower().get(0));
	assertEquals(Rank.KING, score.getCardsPower().get(1));
	assertEquals(Rank.QUEEN, score.getCardsPower().get(2));
	assertEquals(Rank.TEN, score.getCardsPower().get(3));
	assertEquals(Rank.TWO, score.getCardsPower().get(4));
    }

    @Test
    public void testFourOfAKindScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.ACE, Suit.CLUBS));
	cards.add(new Card(Rank.ACE, Suit.DIAMONDS));
	cards.add(new Card(Rank.ACE, Suit.SPADES));
	cards.add(new Card(Rank.ACE, Suit.HEARTS));
	cards.add(new Card(Rank.TEN, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.FOUR_OF_A_KIND, score.getHandPower());
	assertEquals(Rank.ACE, score.getCardsPower().get(0));
	assertEquals(Rank.TEN, score.getCardsPower().get(1));
    }

    @Test
    public void testThreeOfAKindScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.KING, Suit.CLUBS));
	cards.add(new Card(Rank.KING, Suit.DIAMONDS));
	cards.add(new Card(Rank.ACE, Suit.SPADES));
	cards.add(new Card(Rank.KING, Suit.HEARTS));
	cards.add(new Card(Rank.TEN, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.THREE_OF_A_KIND, score.getHandPower());
	assertEquals(Rank.KING, score.getCardsPower().get(0));
	assertEquals(Rank.ACE, score.getCardsPower().get(1));
	assertEquals(Rank.TEN, score.getCardsPower().get(2));
    }

    @Test
    public void testFullHouseScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.KING, Suit.CLUBS));
	cards.add(new Card(Rank.KING, Suit.DIAMONDS));
	cards.add(new Card(Rank.ACE, Suit.SPADES));
	cards.add(new Card(Rank.KING, Suit.HEARTS));
	cards.add(new Card(Rank.ACE, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.FULL_HOUSE, score.getHandPower());
	assertEquals(Rank.KING, score.getCardsPower().get(0));
	assertEquals(Rank.ACE, score.getCardsPower().get(1));
    }

    @Test
    public void testPairScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.KING, Suit.CLUBS));
	cards.add(new Card(Rank.JACK, Suit.DIAMONDS));
	cards.add(new Card(Rank.ACE, Suit.SPADES));
	cards.add(new Card(Rank.KING, Suit.HEARTS));
	cards.add(new Card(Rank.TEN, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.PAIR, score.getHandPower());
	assertEquals(Rank.KING, score.getCardsPower().get(0));
	assertEquals(Rank.ACE, score.getCardsPower().get(1));
	assertEquals(Rank.JACK, score.getCardsPower().get(2));
	assertEquals(Rank.TEN, score.getCardsPower().get(3));
    }

    @Test
    public void testTwoPairsScore() {
	List<Card> cards = new ArrayList<Card>();
	cards.add(new Card(Rank.KING, Suit.CLUBS));
	cards.add(new Card(Rank.JACK, Suit.DIAMONDS));
	cards.add(new Card(Rank.ACE, Suit.SPADES));
	cards.add(new Card(Rank.KING, Suit.HEARTS));
	cards.add(new Card(Rank.JACK, Suit.CLUBS));
	HandScore score = HandScoreCalculator.calculateHandScore(cards);
	assertEquals(HandPower.TWO_PAIRS, score.getHandPower());
	assertEquals(Rank.KING, score.getCardsPower().get(0));
	assertEquals(Rank.JACK, score.getCardsPower().get(1));
	assertEquals(Rank.ACE, score.getCardsPower().get(2));
    }

}
